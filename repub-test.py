#!/usr/bin/python3 

import repubmqtt
import json
import sys


DBG = 0

log=repubmqtt.log

#
# 
REQUIRED_NAMES = [ 'RULES', 'XLATE']

def publish_mqtt(publish, output_data, data):
	topic = publish['topic'] % data
	retain = publish.get('retain',False)
	if publish['_testmode']:
		repubmqtt.log('test', "publish_mqtt %s %s" % (topic, output_data))
		return
	log('error', "mqtt not configured outside testmode")


def main():
	global DBG
	if not len(sys.argv) in [2, 3]:
		log("usage", "%s <conffile> <testdatafile>")
		return 1

	conffile = sys.argv[1]
	if len(sys.argv) == 3:
		testdatafile = sys.argv[2]
	else:
		testdatafile = None

	conf = {}
	conf['DBG'] = DBG
	try:
		exec(open(conffile).read(), conf )
	except Exception as e:
		log('error', "Load of config failed: %s" % e)
		return(1)
	
	err = False
	for name in REQUIRED_NAMES: 
		if not name in conf:
			log('error', "required entry '%s' missing in config file '%s'" % (name, conffile))
			err = True
	if err:
		sys.exit(1)
	DBG = conf['DBG']
	
	repub = repubmqtt.Republish(conf['RULES'], conf['XLATE'])
	repub.setdebuglevel(conf['DBG'])
	repub.settestmode(conf['TESTMODE'])
	repub.register_publish_protocol('mqtt', publish_mqtt)
	
	if testdatafile:
		testdata = open(testdatafile, "r").readlines()
	else:
		testdata = conf['TESTDATA']

	for l in testdata:
		l = l.rstrip('\n')
		if len(l.lstrip()) == 0 or l.lstrip()[0] == "#":
			continue

		topic, msg = l.lstrip().split(None,1)
		try:
			jrecord = json.loads(msg)
		except Exception as e:
			jrecord = {'rawvalue': msg }
			if DBG: log('DBG', "payload not json '%s':  %s" %(e, str(msg)))
		jrecord['topic'] = topic
		repub.process_message(jrecord)
	return 0


if __name__ == "__main__":
	sys.exit(main())
